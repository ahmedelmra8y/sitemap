<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    <url>
        <loc>{{url('/')}}/</loc>
        <lastmod>2021-10-28T21:58:11+00:00</lastmod>
    </url>
    <url>
        <loc>{{url('/travel')}}/</loc>
        <lastmod>2021-10-28T21:58:11+00:00</lastmod>
    </url>
    <url>
        <loc>{{url('/fashion')}}/</loc>
        <lastmod>2021-10-28T21:58:11+00:00</lastmod>
    </url>
    <url>
        <loc>{{url('/art')}}/</loc>
        <lastmod>2021-10-28T21:58:11+00:00</lastmod>
    </url>
</urlset>
