<!-- meta data-->
<title>{{$title}}</title>
<link rel="canonical" href="{{Request::url()}}/">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="canonical_tag" content="{{Request::url()}}/">
<meta name="title" content="{{$title}}"/>
<meta name="description" content="{{$desc}}"/>
<meta property="og:url" content="{{Request::url()}}/"/>
<meta property="og:type" content="website"/>
<meta property="og:title" content="{{$title}}"/>
<meta property="og:description" content="{{$desc}}"/>
<meta property="og:image" content="{{$img}}"/>
<meta name="twitter:card" content="summary">
<meta name="twitter:title" content="{{$title}}">
<meta name="twitter:description" content="{{$desc}}">
<meta name="twitter:url" content="{{Request::url()}}/">
<meta name="twitter:image" content="{{$img}}">
<!-- meta data-->
