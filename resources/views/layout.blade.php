<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @yield('meta')
</head>
<body>
<nav>
    <a href="{{url('/')}}/" class="w3-bar-item w3-button">home</a>
    <a href="{{url('/travel')}}/" class="w3-bar-item w3-button">travel</a>
    <a href="{{url('/art')}}/" class="w3-bar-item w3-button">art</a>
    <a href="{{url('/fashion')}}/" class="w3-bar-item w3-button">fashion</a>
</nav>
@yield('content')
</body>
</html>
