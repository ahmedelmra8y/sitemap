<?php

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    $title = 'cruise travel agent';
    $desc = 'Most people agree travelling is a good thing. People think it’s exciting, almost invigorating to travel. Travelling is the best way to learn new things, whether it be a new language or new culture because you’re experiencing it first hand instead of just reading or looking at pictures. It broadens your view of the world. For me travelling means stress and anxiety (both of which I try to block out)';
    $img = asset('assets/travel.jpeg');
    return view('travel', compact('title', 'desc', 'img'));
});
Route::get('/travel', function () {
    $title = 'cruise travel agent';
    $desc = 'Most people agree travelling is a good thing. People think it’s exciting, almost invigorating to travel. Travelling is the best way to learn new things, whether it be a new language or new culture because you’re experiencing it first hand instead of just reading or looking at pictures. It broadens your view of the world. For me travelling means stress and anxiety (both of which I try to block out)';
    $img = asset('assets/travel.jpeg');
    return view('travel', compact('title', 'desc', 'img'));
});
Route::get('/fashion', function () {
    $title = 'important to follow fashion.';
    $desc = 'Fashion means something that is trendy or valued in a certain time period. The prevailing fashion/ trends/style is called ‘vogue’.
Fashion can also simply mean our lifestyle: the clothing and accessories that we wear and the cosmetics that we apply.
Besides clothing, ornaments, accessories, and make up, it also includes our mannerism and behavior. The way we talk, the words that we use while talking, the voice tone, the clothing, the way we wear the clothes, etc. are all aspects of fashion';
    $img = asset('assets/fashion.jpeg');
    return view('fashion', compact('title', 'desc', 'img'));
});
Route::get('/art', function () {
    $title = 'visual deco arts';
    $desc = 'Art expresses emotions or is an expression of life. Art is a creation that allows for interpretation of any kind. I have read that art is considered a human skill as opposed to nature, a skill applied to music, painting, poetry etc. I believe that nature is an art as well. If something is made by someone or by nature in a certain way, then it’s unique and special in its own way.';
    $img = asset('assets/art.jpeg');
    return view('art', compact('title', 'desc', 'img'));
});
Route::get('/sitemap', function () {
    return Response::view('sitemap')->header('Content-Type', 'application/xml');
});
